<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['middleware' => ['api.auth:api']], function () {
    Route::get('/tamu', 'API\TamuController@index');
    Route::get('/guest_list', 'API\TamuController@guest_list');
    Route::get('/get_event', 'API\EventApiController@get_event');

});
Route::post('login', 'API\AuthController@login');
Route::get('cek', function () {
    $data = array("status" => "ok",
        "message" => "Magic happen here :)");
    return $data;
});



