<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('partials/master');
//});
use Illuminate\Support\Facades\Route;

Route::get('/login', 'Cms\MenuController@index');
Auth::routes();
Route::group(['middleware' => ['auth']], function () {
    Route::get('/', 'Cms\TamuController@index');
    Route::post('/master/post/tamu', 'Cms\TamuController@post_add_tamu')->name('post_add_tamu');
    Route::post('/master/post/bomb_email', 'Cms\TamuController@bomb_email')->name('email_blast');
    Route::get('/home', 'Cms\TamuController@index')->name('home');
    Route::get('/master/add/tamu', 'Cms\TamuController@index')->name('home');
    Route::get('/master/view/edit_tamu/{id}', 'Cms\TamuController@view_edit_tamu')->name('home');
    Route::post('/master/post/edit_tamu', 'Cms\TamuController@post_edit_tamu')->name('post_edit_tamu');
    Route::get('master/delete/tamu/{id}', 'Cms\TamuController@delete_tamu');
    Route::get('/master/list/tamu', 'Cms\TamuController@view_list_tamu');
    Route::get('/master/boarding_pass', 'Cms\TamuController@view_boarding_pass');
    Route::get('/master/download/{id}', 'Cms\TamuController@download_pdf');

    Route::get('/master/add/event', 'Cms\EventController@view_add_event');
    Route::post('/master/post/event', 'Cms\EventController@post_add_event')->name('post_add_event');
    Route::get('/master/delete/event/{id}', 'Cms\EventController@post_delete_event')->name('post_delete_event');
    Route::post('/master/edit/event', 'Cms\EventController@post_edit_event')->name('post_edit_event');
    Route::get('/master/view/edit-event/{id}', 'Cms\EventController@view_edit_event')->name('view_edit_event');

    Route::get('/email/blast/{id}', 'Cms\EventController@email_blast');

    /*email template*/
    Route::get('/master/view/emailtemplate', 'Cms\TemplateController@view_email_template')->name('view_edit_event');
    Route::post('/master/post/emailtemplate', 'Cms\TemplateController@post_email_template')->name('post_email_template');


});
