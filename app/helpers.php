<?php
/**
 * Created by PhpStorm.
 * User: bama
 * Date: 8/1/2019
 * Time: 9:07 PM
 */
function fail_message()
{
    return $data = array("status" => false,
        "message" => "cek your parameter",
        "kode" => 401,
        "result" => []);
}

function message($status = true, $message = 'message', $result = [], $kode = 200)
{
    return $data = array("status" => $status,
        "message" => $message,
        "kode" => $kode,
        "result" => $result);
}