<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $_id
 * @property string $nama
 * @property string $email
 * @property string $role
 * @property string $status
 */
class MasterTamu extends Model
{
    protected $table = 'tamu';
    protected $primaryKey = '_id';

    /**
     * @var array
     */
    protected $fillable = ['nama',
        'email',
        'role',
        'hp',
        'status'];
    //
}
