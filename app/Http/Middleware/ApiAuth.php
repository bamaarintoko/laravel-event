<?php

namespace App\Http\Middleware;

use Closure;

class ApiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //        die('qweee');
        if (!$request->expectsJson()) {
            $data = array("status" => false,
                "message" => "Unauthorized.");
            return response()->json($data);
        }
        return $next($request);
    }
}
