<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use App\Http\Models\Event;
use App\Http\Models\MasterTamu;
use App\Http\Models\Template;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Barryvdh\DomPDF\Facade as PDF;

class EventController extends Controller
{
    public function view_add_event(Request $request)
    {
        $event = Event::get();

        return View('masterdata.event.add-event', ["data" => $event]);
    }

    public function view_edit_event(Request $request)
    {
        //        return $request->id;
        $event   = Event::findOrFail($request->id);
        $val     = explode(" ", $event->waktu);
        $tanggal = $val[0];
        $jam     = $val[1];
        //        return $tanggal;
        //        return $event;
        return View('masterdata.event.edit-event', ["data" => $event,
            'tanggal' => $tanggal,
            'jam' => $jam]);
    }

    public function post_edit_event(Request $request)
    {
        $input = $request->all();
        //        return $input;
        if (isset($input['status'])) {
            $status = 'active';
        } else {
            $status = 'not-active';
        }

        $validator = Validator::make($input, ['nama_event' => 'required',
            'lokasi_event' => 'required']);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        Event::where('id', '!=', $input['id_event'])->update(['status' => 'not-active']);
        $event = Event::findOrFail($input['id_event']);

        //        return $input['tanggal_event'] . " " . $input['jam_event'];
        $event->nama_event = $input['nama_event'];
        $event->waktu      = $input['tanggal_event'] . " " . $input['jam_event'];
        $event->lokasi     = $input['lokasi_event'];
        $event->status     = $status;
        $event->logo       = "-";
        $event->toc        = $input['editor-full'];

        if ($event->update()) {

            return back()->with('message', 'update event berhasil.');
        } else {
            return back()->with('message', 'update event gagal.');
        }
    }

    public function post_add_event(Request $request)
    {

        $input     = $request->all();
        $validator = Validator::make($input, ['nama_event' => 'required',
            'lokasi_event' => 'required']);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $event = new Event();

        //        return $input['tanggal_event'] . " " . $input['jam_event'];
        $event->nama_event = $input['nama_event'];
        $event->waktu      = $input['tanggal_event'] . " " . $input['jam_event'];
        $event->lokasi     = $input['lokasi_event'];
        $event->status     = "not-active";
        $event->logo       = "-";
        $event->toc        = $input['editor-full'];

        if ($event->save()) {
            return back()->with('message', 'Tambah event berhasil.');
        } else {
            return back()->with('message', 'Tambah event gagal.');
        }


    }

    public function post_delete_event(Request $request)
    {
        $id    = $request->id;
        $event = Event::findOrFail($id);
//                return $event;
        if ($event) {
            if ($event->status === 'active') {
                return back()->with('error', 'hapus event gagal.');
            } else {
                if ($event->delete()) {
                    return back()->with('message', 'hapus event berhasil.');
                } else {
                    return back()->with('error', 'hapus event gagal.');

                }
            }
        } else {
            return back()->with('error', 'hapus event gagal.');

        }
        //        return $event;
        //        die('qwe');
        //        return $id;
    }

    public function email_blast(Request $request)
    {

        $guest          = MasterTamu::findOrFail($request->id);
        $template       = Template::first();
        $event          = Event::where("status", "active")->first();
        $time           = Carbon::now();
        $time->timezone = 'Asia/Jakarta';
        $time->toDateTimeString();

        $guest->deliver_at = $time;
        $guest->update();

        /*<Recepiet Info>*/
        $to_name  = $guest->nama;
        $to_email = $guest->email;
        $data     = array('name' => $guest->nama,
            "body" => $template ? $template->text : "Email invitation " . $event->nama_event);
        /*</Recepiet Info>*/

        /*<GeneratePdf>*/

        $view     = View('masterdata.boarding-pass-two', ["qr" => $guest->_id,
            "data" => $guest,
            "event" => $event]);
        $contents = $view->render();
        $pdf      = App::make('dompdf.wrapper');
        $pdf->loadHTML($contents);
        $output = $pdf->output();
        /*</GeneratePdf>*/

        /*EMAIL SENT*/
        Mail::send('email.mail', $data, function ($message) use ($to_name, $to_email, $request, $guest, $event, $output) {
            $message->to($to_email, $to_name)->subject('Hartono Mall Invitation');
            $message->from('no-reply@mail.com', 'Hartono Mall Jogja');
            $message->attachData($output, 'boarding-pass-' . str_replace(" ", "-", strtolower($guest->nama)) . '.pdf', ['mime' => 'application/pdf',]);
        });

        if (Mail::failures()) {
            return back()->with('error', 'sent email to ' . $to_email . ' failed!');
        }
        return back()->with('success', 'email sent to ' . $to_email . ' successfully!');

    }
    //
}
