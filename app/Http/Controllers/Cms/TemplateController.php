<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use App\Http\Models\Template;
use Illuminate\Http\Request;

class TemplateController extends Controller
{
    public function view_email_template()
    {
        $template = Template::first();
        //return $template->text;
        return View('masterdata.template.add-email-template', ["data" => $template ? $template->text : "Email invitation."]);
    }

    public function post_email_template(Request $request)
    {
        $input = $request->all();
        if (!isset($input['editor-full']))
            return back()->with('error', 'template can not be empty');

        $template = Template::first();
        if ($template) {
            //            $tmp = new Template();
            $template->text = $input['editor-full'];
            $template->update();
        } else {
            $tmp       = new Template();
            $tmp->text = $input['editor-full'];
            $tmp->save();
        }
        return back()->with('success', 'template successfully changed');
        //        return $template;
        //        return $input;
    }

}
