<?php

namespace App\Http\Controllers\Cms;

use App\Http\Controllers\Controller;
use App\Http\Models\Event;
use App\Http\Models\MasterTamu;
use App\Http\Models\Template;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class TamuController extends Controller
{
    //

    public function index()
    {
        //        return Auth::user()->name;
        return View('masterdata.tamu.add-tamu');
    }

    public function view_edit_tamu(Request $request)
    {
        $guest = MasterTamu::findOrFail($request->id);
        //        return $guest;
        return View('masterdata.tamu.edit-tamu', ["data" => $guest]);
    }

    public function post_edit_tamu(Request $request)
    {
        $input     = $request->all();
        $validator = Validator::make($input, ['nama' => 'required',
            'email' => 'required|email',
            'hp' => 'required',
            'type' => 'required']);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }
        $tamu = MasterTamu::findOrFail($input['id']);

        $tamu->nama  = ucwords($input['nama']);
        $tamu->email = $input['email'];
        $tamu->hp    = $input['hp'];
        $tamu->role  = $input['type'];
        if ($tamu->update()) {
            return back()->with('message', 'edit tamu berhasil.');
        } else {
            return back()->with('message', 'edit tamu gagal.');
        }
    }

    public function post_add_tamu(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, ['nama' => 'required',
            'email' => 'required|email|unique:tamu,email',
            'hp' => 'required',
            'type' => 'required']);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $tamu         = new MasterTamu();
        $tamu->nama   = ucwords($input['nama']);
        $tamu->email  = $input['email'];
        $tamu->role   = $input['type'];
        $tamu->hp     = $input['hp'];
        $tamu->status = 'non-active';

        if ($tamu->save()) {
            return back()->with('message', 'Tambah tamu berhasil.');
        } else {
            return back()->with('message', 'Tambah tamu gagal.');
        }
    }

    public function view_list_tamu(Request $request)
    {
        $event = Event::where('status', 'active')->first();
        if ($request->nama === "asc") {
            $tamu = MasterTamu::orderBy('nama', 'asc')->paginate(50);
            return View('masterdata.tamu.list-tamu', ["data" => $tamu,
                "event" => $event,
                "nama" => "desc",
                "tgl" => "asc"]);
        }

        if ($request->nama === "desc") {
            $tamu = MasterTamu::orderBy('nama', 'desc')->paginate(50);
            return View('masterdata.tamu.list-tamu', ["data" => $tamu,
                "event" => $event,
                "nama" => "asc",
                "tgl" => "asc"]);
        }

        if ($request->tgl === "desc") {
            $tamu = MasterTamu::orderBy('created_at', 'desc')->paginate(50);
            return View('masterdata.tamu.list-tamu', ["data" => $tamu,
                "event" => $event,
                "nama" => "asc",
                "tgl" => "asc"]);
        }
        if ($request->tgl === "asc") {
            $tamu = MasterTamu::orderBy('created_at', 'asc')->paginate(50);
            return View('masterdata.tamu.list-tamu', ["data" => $tamu,
                "event" => $event,
                "nama" => "asc",
                "tgl" => "desc"]);
        }


        //        $master_inventory = MasterInventory::when($request->keyword, function ($query) use ($request) {
        //            $query->where('nama_barang', 'like', "%{$request->keyword}%");
        //        })->paginate(10);
        $tamu = MasterTamu::when($request->keyword, function ($query) use ($request) {
            $query->where('nama', 'like', "%{$request->keyword}%");
        })->paginate(50);

        $tamu->appends(['keyword'=>$request->keyword]);

        //        return $event;
        return View('masterdata.tamu.list-tamu', ["data" => $tamu,
            "event" => $event,
            "nama" => "asc",
            "tgl" => "asc"]);
        //        return $tamu;
    }

    public function view_boarding_pass()
    {
        //        die("qwe");
        //        $pdf = PDF::loadview('masterdata.boarding-pass');
        //        return $pdf->download('boarding-pass.pdf');
        $event = Event::where("status", "active")->first();
        //                return $event;
        $tamu = MasterTamu::findOrFail(3);
        //        return "qwe";
        $data = array("id" => 1,
            "nama" => $tamu->nama,
            "email" => $tamu->email,
            "type" => $tamu->rol);
        //        return View('masterdata.boarding-pass', ["qr" => 1,
        return View('masterdata.boarding-pass-two', ["qr" => 1,
            "data" => $tamu,
            "event" => $event]);
    }

    public function download_pdf(Request $request)
    {
        $tamu  = MasterTamu::findOrFail($request->id);
        $event = Event::where("status", "active")->first();
        //        $data = array("id"=>1,"nama"=>$tamu->nama,"email"=>$tamu->email,"type"=>$tamu->role);
        //        return $data;
        //        $data = array("id"=>$tamu->_id,"nama"=>$tamu->nama,"email"=>$tamu->email,"type"=>$tamu->role);
        //        $data = array("id"=>$tamu->_id);
        //        return json_encode($data);
        if ($event) {
            $pdf = PDF::loadview('masterdata.boarding-pass-two', ["qr" => $tamu->_id,
                "data" => $tamu,
                "event" => $event]);
            return $pdf->download('boarding-pass-' . str_replace(" ", "-", strtolower($tamu->nama)) . '.pdf');

        } else {
            return back()->with('message', 'Tidak ada event yang aktif');
        }
        //        return View('masterdata.boarding-pass');
        //        return $tamu;
        //        die("qwe");
    }

    public function bomb_email(Request $request)
    {
        $input = $request->all();

        $event          = Event::where("status", "active")->first();
        $time           = Carbon::now();
        $time->timezone = 'Asia/Jakarta';
        $time->toDateTimeString();
        //        return $input;
        if (isset($input['id'])) {
            for ($i = 0; $i < count($input['id']); $i++) {
                $guest    = MasterTamu::findOrFail($input['id'][$i]);
                $template = Template::first();
                /*<Recepiet Info>*/
                $to_name  = $guest->nama;
                $to_email = $guest->email;
                $data     = array('name' => $guest->nama,
                    "body" => $template ? $template->text : "Email invitation " . $event->nama_event);
                /*</Recepiet Info>*/

                /*<GeneratePdf>*/

                $view     = View('masterdata.boarding-pass-two', ["qr" => $guest->_id,
                    "data" => $guest,
                    "event" => $event]);
                $contents = $view->render();
                $pdf      = App::make('dompdf.wrapper');
                $pdf->loadHTML($contents);
                $output = $pdf->output();
                /*</GeneratePdf>*/
                /*EMAIL SENT*/
                Mail::send('email.mail', $data, function ($message) use ($to_name, $to_email, $request, $guest, $event, $output) {
                    $message->to($to_email, $to_name)->subject('Hartono Mall Invitation');
                    $message->from('no-reply@mail.com', 'Hartono Mall Jogja');
                    $message->attachData($output, 'boarding-pass-' . str_replace(" ", "-", strtolower($guest->nama)) . '.pdf', ['mime' => 'application/pdf',]);
                });
                $guest->deliver_at = $time;
                $guest->update();
                if (Mail::failures()) {
                    //                die("qwe");
                }
                echo $guest->email;
            }
            //        return $request['id'];
            return back()->with('success', 'email sent.');
        } else {
            return back()->with('error', 'select user to send email');
        }
    }

    public function delete_tamu(Request $request)
    {
        $input = $request->id;

        $guest = MasterTamu::findOrFail($input);
        if ($guest->delete()) {
            return back()->with('message', 'Hapus tamu berhasil.');
        } else {
            return back()->with('message', 'Hapus tamu gagal.');
        }
        //        return $input;
    }

}
