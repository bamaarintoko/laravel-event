<?php
/**
 * Created by PhpStorm.
 * User: bama
 * Date: 8/2/2019
 * Time: 10:07 PM
 */

namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;
use App\Http\Models\Event;

class EventApiController extends Controller
{
    public function get_event()
    {
        $event = Event::where('status', 'active')->first();
        if ($event) {
           return message(true, 'event available', $event, 200);
        } else {
           return message(false, 'event not available', [], 500);
        }
//        return $event;
    }
}