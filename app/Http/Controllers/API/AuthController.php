<?php
/**
 * Created by PhpStorm.
 * User: bama
 * Date: 8/1/2019
 * Time: 5:56 PM
 */

namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $input = $request->all();
        if (Auth::attempt(['email' => request('email'),
            'password' => request('password')])) {
            $user = Auth::user();

            $user->update();
            $success['token'] = $user->createToken('nApp')->accessToken;
            $success['user']  = $user;
            $data["status"]   = true;
            $data["message"]  = "login success";
            $data["result"]   = $success;
            $data["kode"]     = 200;

        } else {
            $data["status"]  = false;
            $data["message"] = "Unauthorized.";
            $data["result"]  = [];
            $data["kode"]    = 401;
        }

        return $data;
    }
}