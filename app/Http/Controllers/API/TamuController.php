<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Models\MasterTamu;
use Illuminate\Http\Request;

class TamuController extends Controller
{
    public function index(Request $request)
    {
        $input = $request->all();
        $tamu  = MasterTamu::where('_id', $input['id'])->first();
        if ($tamu) {
            if ($tamu->status === "active") {
                return message(false, "undangan sudah di scan", [], 402);
            } else {
                $tamu->status = 'active';
                $tamu->update();
                return message(true, "data ditemukan", $tamu, 200);
            }

        } else {
            return message(false, "data tidak ditemukan", [], 402);
        }
    }

    public function guest_list()
    {
        $guest = MasterTamu::get();

        if (count($guest) > 0) {
            return message(true, "success", $guest, 200);
        } else {
            return message(false, 'no data', [], 401);
        }
    }
    //
}
