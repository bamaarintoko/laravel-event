@extends("partials.master")
@section('main-header')
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><span class="font-weight-semibold">Master Data</span> - Edit tamu</h4>
                {{--<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>--}}
            </div>

            <div class="header-elements d-none">
                {{--<div class="d-flex justify-content-center">--}}
                {{--<a href="#" class="btn btn-link btn-float text-default"><i class="mi-person-add mi-2x text-primary"></i>--}}
                {{--<span>Tambah Guru</span></a>--}}
                {{--</div>--}}
            </div>
        </div>


    </div>
@endsection
@section('main-content')
    <div class="col-md-6">
        <!-- Basic layout-->
        <div class="card">
            <div class="card-body">
                @if (count($errors) > 0)
                    <div class="alert alert-warning alert-bordered">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if (session('message'))
                    <div class="alert alert-info alert-bordered">
                        {{session('message')}}
                    </div>
                @endif
                <form action="{{route('post_edit_tamu')}}" method="POST">
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Nama:</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" placeholder="Nama" name="nama"
                                   value="{{$data->nama}}">
                            <input type="hidden" class="form-control" placeholder="Nama" name="id"
                                   value="{{$data->_id}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Type:</label>
                        <div class="col-lg-9">
                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-input-styled" name="type" value="vip"
                                           {{ ($data->role=="vip")? "checked" : "" }}
                                           data-fouc>
                                    Vip
                                </label>
                            </div>

                            <div class="form-check form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-input-styled" name="type" value="non-vip"
                                            {{ ($data->role=="non-vip")? "checked" : "" }}>
                                    Non Vip
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Email:</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" placeholder="Email" name="email"
                                   value="{{$data->email}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Hp:</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" placeholder="Hp" name="hp" value="{{$data->hp}}">
                        </div>
                    </div>
                    @csrf
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">Submit form <i class="icon-paperplane ml-2"></i>
                        </button>
                    </div>
                </form>
            </div>
        </div>

    </div>
@endsection