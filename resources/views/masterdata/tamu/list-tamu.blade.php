@extends("partials.master")
@section('main-header')
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><span class="font-weight-semibold">Master Data</span> - Daftar Tamu</h4>
                {{--<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>--}}
            </div>

            <div class="header-elements d-none">
                {{--<div class="d-flex justify-content-center">--}}
                {{--<a href="#" class="btn btn-link btn-float text-default"><i class="mi-person-add mi-2x text-primary"></i>--}}
                {{--<span>Tambah Guru</span></a>--}}
                {{--</div>--}}
            </div>
        </div>


    </div>
@endsection
@section('main-content')
    <div class="col-md-12">
        <!-- Basic layout-->
        <div class="card">
            <div class="card-header">
                <h1>Daftar tamu <strong>{{$event->nama_event}}</strong></h1>
                <form class="header-elements-inline" action="{{url()->current()}}" method="GET">
                    {{--<div class="form-group row">--}}
                    <div class="col-lg-3">
                        <input value="{{old("keyword")}}" type="text" name="keyword" class="form-control" placeholder="Pencarian">
                    </div>
                    {{--</div>--}}
                    <div class="col-lg-9">
                        <button type="submit" class="btn btn-primary"><i class="icon-search4"></i></button>
                    </div>
                </form>
            </div>
            <div class="card-body">
                @if (session('message'))
                    <div class="alert alert-info alert-bordered">
                        {{session('message')}}
                    </div>
                @endif
                @if (session('success'))
                    <div class="alert alert-info alert-bordered">
                        {{session('success')}}
                    </div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger alert-bordered">
                        {{session('error')}}
                    </div>
                @endif

                @if(count($data)>0)
                    <div class="table-responsive">
                        <form action="{{route('email_blast')}}" method="POST">
                            @csrf
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th width="30">No</th>
                                    <th width="300">Email</th>
                                    <th><a href="/master/list/tamu?nama={{$nama}}" class="btn btn-link">Nama</a></th>
                                    <th width="100">Type</th>
                                    <th width="200">Email Delivery Time
                                    </th>
                                    <th width="100">Status</th>
                                    <th width="170"><a href="/master/list/tamu?tgl={{$tgl}}" class="btn btn-link">Create Date</a></th>
                                    <th width="200">##</th>
                                    <th width="100">
                                        <div class="custom-control custom-checkbox custom-control-inline">
                                            <input type="checkbox" class="custom-control-input"
                                                   name="select-all"
                                                   id="select-all">
                                            <label class="custom-control-label"
                                                   for="select-all"></label>
                                        </div>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $x = ($data->currentPage() - 1) * $data->perPage() + 1

                                ?>
                                @for ($i = 0; $i < count($data); $i++)

                                    <tr>
                                        <td>{{$x++}}</td>
                                        <td>{{$data[$i]->email}}</td>
                                        <td>{{$data[$i]->nama}}</td>
                                        <td>
                                            <span class="badge {{$data[$i]->role==='vip'?'badge-primary':'badge-secondary'}} ">{{$data[$i]->role}}</span>
                                        </td>
                                        <td>
                                            @if($data[$i]->deliver_at===null)
                                                -
                                            @else
                                                {{$data[$i]->deliver_at}}
                                            @endif
                                        </td>
                                        <td>
                                            <span class="badge {{$data[$i]->status==='non-active'?'badge-warning':'badge-success'}} ">{{$data[$i]->status}}</span>

                                        </td>
                                        <td>
                                            {{$data[$i]->created_at}}

                                        </td>
                                        <td>

                                            <a href="/master/download/{{$data[$i]->_id}}"
                                               class="btn btn-outline bg-primary border-primary text-primary-800 btn-icon"><i
                                                        class="icon-file-download"></i></a>
                                            <a href="/master/view/edit_tamu/{{$data[$i]->_id}}"
                                               class="btn btn-outline bg-primary border-primary text-primary-800 btn-icon">
                                                <i class="icon-pencil7"></i></a>
                                            <a href="/email/blast/{{$data[$i]->_id}}"
                                               class="btn btn-outline bg-primary border-primary text-primary-800 btn-icon">
                                                <i class="icon-envelop5"></i></a>
                                            <button type="button"
                                                    class="btn btn-outline bg-primary border-primary text-primary-800 btn-icon"
                                                    data-toggle="modal" data-target="#modal_default{{$data[$i]->_id}}">
                                                <i
                                                        class="icon-trash"></i></button>


                                        </td>
                                        <td>
                                            <div class="custom-control custom-checkbox custom-control-inline">
                                                <input type="checkbox" class="custom-control-input"
                                                       name="id[]"
                                                       value="{{$data[$i]->_id}}"
                                                       id="custom_checkbox_inline_unchecked{{$i}}">
                                                <label class="custom-control-label"
                                                       for="custom_checkbox_inline_unchecked{{$i}}"></label>
                                            </div>
                                        </td>
                                    </tr>
                                    <!-- Basic modal -->
                                    <div id="modal_default{{$data[$i]->_id}}" class="modal fade" tabindex="-1">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Konfirmasi hapus tamu</h5>
                                                    <button type="button" class="close" data-dismiss="modal">&times;
                                                    </button>
                                                </div>

                                                <div class="modal-body">
                                                    <p>Hapus tamu <strong>{{$data[$i]->nama}}</strong>?</p>
                                                    {{--<input type="text" class="form-control" placeholder="Nama" name="delete"--}}
                                                    {{--value="{{$data[$i]->_id}}">--}}
                                                </div>

                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-link" data-dismiss="modal">
                                                        Close
                                                    </button>
                                                    <a href="/master/delete/tamu/{{$data[$i]->_id}}"
                                                       class="btn btn-outline bg-primary border-primary text-primary-800 btn-icon">Hapus</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /basic modal -->
                                @endfor
                                <tr>
                                    <td colspan="8"></td>
                                    <td>
                                        <button type="submit"
                                                class="btn btn-outline bg-primary border-primary text-primary-800 btn-icon">
                                            <i class="icon-envelop5"></i> Send Email
                                        </button>
                                    </td>

                                </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>
                            {{ $data->links() }}
            </div>
            @else
                <div class="card-body">
                    <div class="alert alert-info alert-bordered">
                        Tidak ada data tamu.
                    </div>
                </div>
            @endif

        </div>

    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function () {

            $('#select-all').click(function (event) {
                // alert("qwe")
                if (this.checked) {
                    // Iterate each checkbox
                    $(':checkbox').each(function () {
                        this.checked = true;
                    });
                } else {
                    $(':checkbox').each(function () {
                        this.checked = false;
                    });
                }
            });
        });
    </script>
@endsection