{{--<link href='https://fonts.googleapis.com/css?family=Biryani' rel='stylesheet'>--}}
<div class="col-md-12">
    <!-- Basic layout-->
    <div class="card">
        <div class="card-body">

            <table class="table table-striped">
                <tr>
                    <td width="100%" colspan="2" valign="top">
                        <div>Event</div><br>
                        <div style="float: right;"><strong style="font-size: 24px; text-align: right">{{$event->nama_event}}</strong></div>
                    </td>
                    <td height="120" rowspan="2">Logo Event</td>
                </tr>
                <tr>
                    <td valign="top">
                        <div>Date + Time</div>
                        <div style="float: right;"><strong style="font-size: 18px; text-align: right">{{$event->waktu}}</strong></div>
                    </td>
                    <td valign="top">
                        <div>Location</div>
                        <div><strong>{{$event->lokasi}}</strong></div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div>Info</div>
                        <div style="float: right; padding-right: 10px"><b>{{$data->nama}}</b> - {{$data->email}}</div>
                    </td>
                    <td rowspan="2"> <img src="data:image/png;base64, {{ base64_encode(QrCode::format('png')->size(100)->generate($qr)) }} "></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div>Type</div>
                        <div style="float: right;">{{$data->role}}</div>
                    </td>
                </tr>
            </table>


            {{--<div class="ticket">--}}
                {{--<header>--}}
                    {{--<div class="company-name">--}}
                        {{--Event--}}
                    {{--</div>--}}
                    {{--<div class="gate">--}}
                        {{--<div>--}}
                            {{--VIP--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</header>--}}
                {{--<section class="airports">--}}
                    {{--<p style="font-size: 30px">BAMA</p>--}}

                {{--</section>--}}
                {{--<section class="place">--}}
                    {{--<div class="qr">--}}
                        {{--<img src="data:image/png;base64, {{ base64_encode(QrCode::format('png')->size(220)->generate($qr)) }} ">--}}
                        {{--                        {!! QrCode::size(220)->generate('Yosafat Bama Arintoko'); !!}--}}
                        {{--<img src="http://www.classtools.net/QR/pics/qr.png"/>--}}
                    {{--</div>--}}
                {{--</section>--}}
            {{--</div>--}}

        </div>

    </div>

</div>
<style>
    @import 'https://fonts.googleapis.com/css?family=Biryani';
    body div {
        font-family: 'Biryani';
    }
    table, th, td {
        border: 5px solid #D2D6DF;
    }
    table{
        max-width: 2480px;
        width:100%;
    }
    /*table td{*/
        /*width: auto;*/
        /*overflow: hidden;*/
        /*word-wrap: break-word;*/
    /*}*/
    table {
        border-collapse: collapse;
    }


</style>