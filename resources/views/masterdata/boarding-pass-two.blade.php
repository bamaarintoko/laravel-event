{{--<link href='https://fonts.googleapis.com/css?family=Biryani' rel='stylesheet'>--}}
<div class="col-md-12">
    <!-- Basic layout-->
    <div class="card">
        <div class="card-body">
            <div class="row" style="height: 100px;">
                <div style="width: 50%; float: left">
                    <img style="height: 80px;" src="{{public_path('assets/logo2.png')}}">
                </div>
                <div style="width: 50%;float: left">
                    <img style="height: 50px; float: right; margin-top: 20px" src="{{public_path('assets/logo3.png')}}">
                </div>
            </div>
            <div>
                <div style="position: absolute;width: 180px; left: 20px; top: 120px">
                    <div style="background-color: #0aa7ef;height: 180px;">
                        <img style="margin:auto"
                             src="data:image/png;base64, {{ base64_encode(QrCode::format('png')->size(180)->generate($qr)) }} "><br>
                    </div>
                    <div style="text-align: center; margin-top: 5px">
                        <strong style="color: #fff">{{$data->role==="vip"?"FRONT ROW":"TRIBUN"}} {{strtoupper($data->role)}}</strong>

                    </div>
                </div>
                <img class="bg" src="{{public_path('assets/logo.png')}}">

            </div>
            {{--</div>--}}
            <div style="text-align: center;letter-spacing: 3px; margin-top: 10px; margin-bottom: 10px">
                <strong style="">{{strtoupper($data->nama)}}</strong>
                | {{Carbon\Carbon::parse($event->waktu)->format('l, d F Y')}}
                | {{Carbon\Carbon::parse($event->waktu)->format('H:i')}} | {{$event->lokasi}}
            </div>
            <table class="table table-striped">
                {{--<tr>--}}
                {{--<td width="100%" valign="top" style="text-align: center">--}}
                {{--<p>TIKET INI BERLAKU UNTUK SATU ORANG </p>--}}
                {{--</td>--}}
                {{--</tr>--}}
                {{--<tr>--}}
                {{--<td width="100%" valign="top" style="text-align: center;">--}}
                {{--<div class="row">--}}
                {{--<div class="column">--}}
                {{--<div style="padding: 10px">--}}

                {{--<strong>{{$data->nama}}</strong>--}}
                {{--<br>--}}
                {{--<strong style="font-size: larger">{{$event->nama_event}}</strong>--}}
                {{--</div>--}}
                {{--<div style="padding: 10px">--}}
                {{--<div>--}}

                {{--<b style="font-size: 18px; text-align: right">{{Carbon\Carbon::parse($event->waktu)->format('l, d F Y H:i')}}</b>--}}
                {{--<br>--}}
                {{--<strong style="font-size: 18px; text-align: right">{{$event->lokasi}}</strong>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="right">--}}
                {{--<div>--}}

                {{--<img src="data:image/png;base64, {{ base64_encode(QrCode::format('png')->size(200)->generate($qr)) }} ">--}}
                {{--</div>--}}
                {{--@if($data->role==='vip')--}}
                {{--<strong style="font-size: x-large;font-weight: bolder">Front Row</strong>--}}
                {{--@else--}}
                {{--<strong style="font-size: x-large;font-weight: bolder">Tribun</strong>--}}
                {{--@endif--}}
                {{--<br>--}}
                {{--<strong style="font-size: x-large;font-weight: bolder">{{$data->role}}</strong>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</td>--}}
                {{--</tr>--}}
                @if($event->toc)
                    <tr>
                        <td width="100%" valign="top" style="text-align: center">
                            <p>SYARAT DAN KETENTUAN </p>
                        </td>
                    </tr>
                    <tr>
                        <td width="100%" valign="top" style="padding: 10">
                            {!! $event->toc !!}
                        </td>
                    </tr>
                @endif
            </table>


        </div>

    </div>

</div>
<style type="text/css" media="all">
    /*@import 'https://fonts.googleapis.com/css?family=Biryani';*/
    {{--@font-face {--}}
        {{--font-family: Biryani-Regular;--}}
        {{--src: url('{{ public_path('fonts/Biryani-Regular.ttf') }}');--}}
    {{--}--}}
    {{--@font-face {--}}
        {{--font-family: 'Biryani';--}}
        {{--src: url({{ storage_path('fonts\Biryani-Regular.ttf') }}) format('truetype');--}}
    {{--}--}}
    body div {
        font-family: 'Nunito', sans-serif;
        margin: 0;
    }

    table, th, td {
        border: 3px solid #000;
    }

    table {
        max-width: 2480px;
        width: 100%;
    }

    /*table td{*/
    /*width: auto;*/
    /*overflow: hidden;*/
    /*word-wrap: break-word;*/
    /*}*/
    table {
        border-collapse: collapse;
    }

    .c {
        float: left;
        width: 30%;
        height: 150px;
    }

    .d {
        float: left;
        width: 40%;
        height: 150px;
    }

    .column {
        float: left;
        width: 70%;
        font-size: 24px;
        text-align: left;
    }

    .right {
        float: left;
        width: 30%;
        align-items: center;
        align-content: center;
        text-align: center;
    }

    /* Clear floats after the columns */
    .row:after {
        content: "";
        clear: both;
    }

    .parent {
        display: flex;
        height: 300px; /* Or whatever */
    }

    .child {
        width: 50%; /* Or whatever */
        height: 50%; /* Or whatever */
        margin: auto; /* Magic! */
    }

    #watermark {
        position: fixed;
        width: 200px;
        height: 200px;
    }

    .bg {
        width: 100%;
        object-fit: cover;
        {{--background-image: url({{url('assets/logo.jpg')}});--}}
            background-repeat: no-repeat;
        background-position: 90% 90%;
    }
</style>