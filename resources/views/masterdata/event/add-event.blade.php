@extends("partials.master")
@section('main-header')
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><span class="font-weight-semibold">Master Data</span> - Event</h4>
                {{--<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>--}}
            </div>

            <div class="header-elements d-none">
                {{--<div class="d-flex justify-content-center">--}}
                {{--<a href="#" class="btn btn-link btn-float text-default"><i class="mi-person-add mi-2x text-primary"></i>--}}
                {{--<span>Tambah Guru</span></a>--}}
                {{--</div>--}}
            </div>
        </div>


    </div>
@endsection
@section('main-content')
    <div class="row">


        <div class="col-md-12">
            <!-- Basic layout-->
            <div class="card">
                <div class="card-body">
                    @if (count($errors) > 0)
                        <div class="alert alert-warning alert-bordered">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (session('message'))
                        <div class="alert alert-info alert-bordered">
                            {{session('message')}}
                        </div>
                    @endif
                    @if (session('error'))
                        <div class="alert alert-warning alert-bordered">
                            {{session('error')}}
                        </div>
                    @endif
                    <form action="{{route('post_add_event')}}" method="POST">
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label">Nama Event:</label>
                            <div class="col-lg-10">
                                <input value="{{ old('nama_event') }}" type="text" class="form-control"
                                       placeholder="Nama Event" name="nama_event">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label">Waktu:</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control daterange-single" name="tanggal_event"
                                       placeholder="Waktu Event">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label">Waktu:</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" id="anytime-time" name="jam_event"
                                       value="{{ old('jam_event') }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label">Lokasi:</label>
                            <div class="col-lg-10">
                                <input type="text" class="form-control" placeholder="Lokasi Event" name="lokasi_event" value="{{ old('lokasi_event') }}">
                            </div>
                        </div>
                        <div class="card-header header-elements-inline">
                            <h5 class="card-title">Syarat dan ketentuan</h5>
                        </div>
                        <div class="mb-3">
								<textarea name="editor-full" id="editor-full" rows="4" cols="4">
                                    {{ old('editor-full') }}
                                </textarea>
                        </div>

                        @csrf
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary">Submit form <i
                                        class="icon-paperplane ml-2"></i>
                            </button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
        <div class="col-md-12">
            <!-- Basic layout-->
            <div class="card">
                @if(count($data)>0)

                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th width="100">No</th>
                                <th width="400">Event</th>
                                <th width="200">Waktu</th>
                                <th width="200">Status</th>
                                <th width="100">##</th>
                            </tr>
                            </thead>
                            <tbody>
                            @for ($i = 0; $i < count($data); $i++)
                                <tr>
                                    <td>{{$i+1}}</td>
                                    <td>{{$data[$i]->nama_event}}</td>
                                    <td>{{$data[$i]->waktu}}</td>
                                    <td>{{$data[$i]->status}}</td>
                                    <td>
                                        <a href="/master/view/edit-event/{{$data[$i]->id}}"
                                           class="btn btn-outline bg-primary border-primary text-primary-800 btn-icon"><i
                                                    class="icon-pencil7"></i></a>
                                        <button type="button"
                                                class="btn btn-outline bg-primary border-primary text-primary-800 btn-icon"
                                                data-toggle="modal" data-target="#modal_default{{$data[$i]->id}}"><i
                                                    class="icon-trash"></i></button>
                                    </td>
                                </tr>
                                <!-- Basic modal -->
                                <div id="modal_default{{$data[$i]->id}}" class="modal fade" tabindex="-1">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Konfirmasi hapus event</h5>
                                                <button type="button" class="close" data-dismiss="modal">&times;
                                                </button>
                                            </div>

                                            <div class="modal-body">
                                                <p>Hapus event <strong>{{$data[$i]->nama_event}}</strong>?</p>
                                                {{--<input type="text" class="form-control" placeholder="Nama" name="delete"--}}
                                                {{--value="{{$data[$i]->_id}}">--}}
                                            </div>

                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-link" data-dismiss="modal">Close
                                                </button>
                                                <a href="/master/delete/event/{{$data[$i]->id}}"
                                                   class="btn btn-outline bg-primary border-primary text-primary-800 btn-icon">Hapus</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /basic modal -->
                            @endfor
                            </tbody>
                        </table>
                    </div>
                @else
                    <div class="card-body">
                        <div class="alert alert-info alert-bordered">
                            Tidak ada data event.
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            if (!$().AnyTime_picker) {
                console.warn('Warning - anytime.min.js is not loaded.');
                return;
            }
            $('.pickadate-selectors').pickadate({
                selectYears: true,
                selectMonths: true,
                showDropdowns: true
            });
            $('.pickadate-limits').pickadate({
                min: [2014, 3, 20],
                max: [2014, 7, 14]
            });
            $('.daterange-single').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                locale: {
                    format: 'YYYY-MM-DD'
                }
            });
            $('#anytime-time').AnyTime_picker({
                format: '%H:%i'
            });
        });
    </script>
@endsection