@extends("partials.master")
@section('main-header')
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><span class="font-weight-semibold">Master Data</span> - Event</h4>
                {{--<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>--}}
            </div>

            <div class="header-elements d-none">
                {{--<div class="d-flex justify-content-center">--}}
                {{--<a href="#" class="btn btn-link btn-float text-default"><i class="mi-person-add mi-2x text-primary"></i>--}}
                {{--<span>Tambah Guru</span></a>--}}
                {{--</div>--}}
            </div>
        </div>


    </div>
@endsection
@section('main-content')
    <div class="col-md-12">
        <!-- Basic layout-->
        <div class="card">
            <div class="card-body">
                @if (count($errors) > 0)
                    <div class="alert alert-warning alert-bordered">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if (session('message'))
                    <div class="alert alert-info alert-bordered">
                        {{session('message')}}
                    </div>
                @endif
                <form action="{{route('post_edit_event')}}" method="POST">
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Nama Event:</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" value="{{$data->nama_event}}" placeholder="Nama Event" name="nama_event">
                            <input type="hidden" class="form-control" value="{{$data->id}}" placeholder="Nama Event" name="id_event">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Waktu:</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control daterange-single" value="{{$tanggal}}" name="tanggal_event"
                                   placeholder="Waktu Event">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Waktu:</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" id="anytime-time" name="jam_event"
                                   value="{{$jam}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Lokasi:</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" value="{{$data->lokasi}}" placeholder="Lokasi Event" name="lokasi_event">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-lg-3 col-form-label">Status:</label>
                        <div class="col-lg-9">
                            <div class="form-check form-check-switch form-check-switch-left">
                                <label class="form-check-label d-flex align-items-center">
                                    <input type="checkbox" name="status" data-on-color="success" data-off-color="danger"
                                           data-on-text="Active" data-off-text="Not-Active"
                                           class="form-check-input-switch" {{$data->status=='active'?'checked':''}}>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">Syarat dan ketentuan</h5>
                    </div>
                    <div class="mb-3">
								<textarea name="editor-full" id="editor-full" rows="4" cols="4">
                                    {{$data->toc}}
                                </textarea>
                    </div>

                    @csrf
                    <div class="text-right">
                        <button type="submit" class="btn btn-primary">Submit form <i
                                    class="icon-paperplane ml-2"></i>
                        </button>
                    </div>
                </form>
            </div>
        </div>

    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            if (!$().AnyTime_picker) {
                console.warn('Warning - anytime.min.js is not loaded.');
                return;
            }
            $('.pickadate-selectors').pickadate({
                selectYears: true,
                selectMonths: true,
                showDropdowns: true
            });
            $('.pickadate-limits').pickadate({
                min: [2014, 3, 20],
                max: [2014, 7, 14]
            });
            $('.daterange-single').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                locale: {
                    format: 'YYYY-MM-DD'
                }
            });
            $('#anytime-time').AnyTime_picker({
                format: '%H:%i'
            });
        });
    </script>
@endsection