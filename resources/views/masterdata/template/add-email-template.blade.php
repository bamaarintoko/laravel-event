@extends("partials.master")
@section('main-header')
    <div class="page-header page-header-light">
        <div class="page-header-content header-elements-md-inline">
            <div class="page-title d-flex">
                <h4><span class="font-weight-semibold">Template</span> - Email</h4>
                {{--<a href="#" class="header-elements-toggle text-default d-md-none"><i class="icon-more"></i></a>--}}
            </div>

            <div class="header-elements d-none">
                {{--<div class="d-flex justify-content-center">--}}
                {{--<a href="#" class="btn btn-link btn-float text-default"><i class="mi-person-add mi-2x text-primary"></i>--}}
                {{--<span>Tambah Guru</span></a>--}}
                {{--</div>--}}
            </div>
        </div>


    </div>
@endsection
@section('main-content')
    <div class="row">

        <div class="col-md-12">
            <!-- Basic layout-->
            <div class="card">
                <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-info alert-bordered">
                            {{session('success')}}
                        </div>
                    @endif
                        @if (session('error'))
                            <div class="alert alert-danger alert-bordered">
                                {{session('error')}}
                            </div>
                        @endif
                    <form action="{{route('post_email_template')}}" method="post">
                        @csrf
                        <div class="mb-3">
								<textarea name="editor-full" id="editor-full" rows="4" cols="4">
                                    {{$data}}
                                </textarea>
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn bg-teal-400">Submit form <i
                                        class="icon-paperplane ml-2"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection