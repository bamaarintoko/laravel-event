<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeliverAtToTamu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tamu', function (Blueprint $table) {
            $table->dateTime('deliver_at')->nullable($value = true)->after('status');
//            $table->dateTime('deliver_at')->default(null)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tamu', function (Blueprint $table) {
            $table->dropColumn('deliver_at');
        });
    }
}
